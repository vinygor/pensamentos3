﻿using Pensamentos.Dados.Imagem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pensamentos
{
    public partial class Perfil : Form
    {
        public Perfil()
        {
            InitializeComponent();
            Carregamento_perfil();
        }

        private void Perfil_Load(object sender, EventArgs e)
        {

        }
        private void Carregamento_perfil()
        {
            string Nome = Static_Class.usuario.nm_nome;
            int Idade = Static_Class.usuario.id_usuario;
            string Imagem = Static_Class.usuario.ds_imagem_perfil;
            picest.Image = ImagemPlugin.ConverterParaImagem(Imagem);
            lblnome.Text = Nome;
            lblidade.Text = Idade.ToString();


            
        }


    }
}
