﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho_Pensamento.Classes.DTO;
using Trabalho_Pensamento.DB;

namespace Trabalho_Pensamento.Classes
{
    public class UsuarioDatabase



    {

        public UsuarioDTO Verificaçao(string nome, string senha)
        {
            string script = @"select * from tb_usuario where nm_nome = @nm_nome and ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", nome));
            parms.Add(new MySqlParameter("ds_senha", senha));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            UsuarioDTO dto = null;


            if (reader.Read() == true)
            {
                dto = new UsuarioDTO();
                dto.id_usuario = reader.GetInt32("id_usuario");
                dto.nm_nome = reader.GetString("nm_nome");
                dto.ds_idade = reader.GetInt32("ds_idade");
            }
            else
            {
                throw new ArgumentException("Senha errada");

            }
            reader.Close();

            return dto;



        }
        public int Salvar(UsuarioDTO dto)
        {
            string script = @"INSERT INTO `bd_pensamento`.`tb_usuario`(nm_nome,ds_idade,ds_senha,ds_imagem_perfil) VALUES (@nm_nome,@ds_idade,@ds_senha,@ds_imagem_perfil)";
            

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("nm_nome", dto.nm_nome));
            parm.Add(new MySqlParameter("ds_idade", dto.ds_idade));
            parm.Add(new MySqlParameter("ds_senha", dto.ds_senha));
            parm.Add(new MySqlParameter("ds_imagem_perfil", dto.ds_imagem_perfil));

            Database database = new Database();
            int pk = database.ExecuteInsertScriptWithPk(script,parm);
            return pk;


        }










    }
}
