﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Pensamento.Classes.DTO
{
    public class UsuarioDTO
    {

        public int id_usuario { get; set; }

        public string nm_nome { get; set; }

        public string ds_senha { get; set; }

        public int ds_idade { get; set; }

        public string ds_imagem_perfil { get; set; }

    }
}