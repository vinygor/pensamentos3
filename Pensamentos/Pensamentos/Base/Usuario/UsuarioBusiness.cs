﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho_Pensamento.Classes.DTO;

namespace Trabalho_Pensamento.Classes.Business
{
    public class UsuarioBusiness
    {
        public UsuarioDTO Verificar(string nome, string senha)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.Verificaçao(nome, senha);

        }
        public int Salvar(UsuarioDTO dto)
        {

            UsuarioDatabase db = new UsuarioDatabase();
            int pk = db.Salvar(dto);
            return pk;

        }

    }
}

