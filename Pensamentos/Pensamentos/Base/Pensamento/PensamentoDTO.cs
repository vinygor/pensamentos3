﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trabalho_Pensamento.Classes.DTO
{
    public class PensamentoDTO
    {

        public int id_pensamento { get; set; }

        public string nm_titulo { get; set; }

        public string ds_frase { get; set; }

        public DateTime dt_data { get; set; }

        public UsuarioDTO id_usuario { get; set; }

        public string ds_imagem_pensamento { get; set; }
    }
}