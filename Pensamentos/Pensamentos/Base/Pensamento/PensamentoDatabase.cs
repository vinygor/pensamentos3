﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho_Pensamento.Classes.DTO;
using Trabalho_Pensamento.DB;

namespace Trabalho_Pensamento.Classes
{
    public class PensamentoDatabase
    {
        public int Salvar(PensamentoDTO dto)
        {
            string script = @"INSERT INTO `bd_pensamento`.`tb_pensamento` (nm_titulo,ds_frase,dt_data,id_usuario,ds_imagem_pensamento) VALUES (@nm_titulo,@ds_frase,@dt_data,@id_usuario,@ds_imagem_pensamento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_titulo", dto.nm_titulo));
            parms.Add(new MySqlParameter("ds_frase", dto.ds_frase));
            parms.Add(new MySqlParameter("dt_data", dto.dt_data));
            parms.Add(new MySqlParameter("id_usuario", dto.id_usuario.id_usuario));
            parms.Add(new MySqlParameter("ds_imagem_pensamento", dto.ds_imagem_pensamento));
            

            Database database = new Database();
            int pk = database.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        
        public List<PensamentoDTO> Listar()
        {
            string script = @"select * from tb_pensamentos";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PensamentoDTO> lista = new List<PensamentoDTO>();
            while (reader.Read())
            {
                PensamentoDTO pensamento = new PensamentoDTO();
                pensamento.id_pensamento = reader.GetInt32("id_pensamento");
                pensamento.nm_titulo = reader.GetString("nm_titulo");
                pensamento.ds_frase = reader.GetString("ds_frase");
                pensamento.dt_data = reader.GetDateTime("dt_data");
                pensamento.id_usuario = new UsuarioDTO();
                pensamento.id_usuario.id_usuario = reader.GetInt32("id_usuario");


                lista.Add(pensamento);
            }
            reader.Close();

            return lista;
        }






    }
}

