﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trabalho_Pensamento.Classes.DTO;

namespace Trabalho_Pensamento.Classes.Business
{
    public class PensamentoBusiness
    {
        public int Salvar(PensamentoDTO dto)
        {
            PensamentoDatabase database = new PensamentoDatabase();
            int pk = database.Salvar(dto);
            return pk;


        }
        public List<PensamentoDTO> Listar()
        {
            PensamentoDatabase db = new PensamentoDatabase();
            List<PensamentoDTO> listar = db.Listar();
            return listar;

        }


    }
}

