﻿using Pensamentos.Dados.Imagem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trabalho_Pensamento.Classes;
using Trabalho_Pensamento.Classes.DTO;

namespace Pensamentos
{
    public partial class Compartilha_Mensagem : Form
    {
        public Compartilha_Mensagem()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            
        }

      
        private void picest_Click(object sender, EventArgs e)
        {
           
        }

        private void btnentrar_Click(object sender, EventArgs e)
        {
            ///dto.Imagem = ImagemPlugin.ConverterParaString(picest.Image);
            ///imgfoto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
            ///


            PensamentoDTO dto = new PensamentoDTO();
            dto.nm_titulo = txttitulo.Text;
            dto.ds_frase = txtpensamento.Text;
            dto.ds_imagem_pensamento= ImagemPlugin.ConverterParaString(picest.Image);
            dto.dt_data = DateTime.Now;
            int id_usuario=Static_Class.usuario.id_usuario;

            PensamentoDatabase database = new PensamentoDatabase();
            database.Salvar(dto);
        }

        private void label3_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                picest.ImageLocation = dialog.FileName;
            }
        }
    }
}
