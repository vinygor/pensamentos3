﻿using Pensamentos.Dados.Imagem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trabalho_Pensamento.Classes.Business;
using Trabalho_Pensamento.Classes.DTO;

namespace Pensamentos
{
    public partial class Criação_Perfil : Form
    {
        public Criação_Perfil()
        {
            InitializeComponent();
        }




        private void btnentrar_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == txtsenha.Text )
            {
                UsuarioDTO dto = new UsuarioDTO();
                dto.nm_nome = txtnome.Text;
                dto.ds_idade =Convert.ToInt32(txtidade.Text);
                dto.ds_senha = txtconfirmaçao.Text;
                dto.ds_imagem_perfil = ImagemPlugin.ConverterParaString(picest.Image);

                UsuarioBusiness business = new UsuarioBusiness();
                business.Salvar(dto);

            }
            else
            {
                MessageBox.Show("Senha e Confirmação nao estão iguais");
            }
        }

        private void Criação_Perfil_Load(object sender, EventArgs e)
        {

        }

        private void picest_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }
    }
}